<?php
	session_start();
	$connect = mysqli_connect("localhost","root","","student");
?>
<!DOCTYPE html>
<html>

<head>
<title>Day12</title>

<style>
input {
  padding: 15px 15px;
  margin: 8px 0;
  box-sizing: border-box;
}
div.b {
  background-color: white;
  padding: 0px;
  border: 2px solid indigo;
  margin: 15px;
}
div.c{
	text-align:center;
}
span.d {
  display:inline-block;
  background-color:limegreen;
  color:white;
  height: 20px;
  width:100px;
  padding: 15px;
  border: 1px solid indigo;
  margin: 15px;
}
div.e{
	text-align:right;
}
.button {
  background-color: limegreen;
  border: 1px solid indigo;
  color: white;
  margin:15px;
  width:100px;
  padding: 15px;
  text-align: center;
  display: inline-block;
  font-size: 16px;
}
table,tr,td{
	border: 0px;
}
th {
  text-align: left;
}
</style>
</head>

<body>
	<form method = "post">
		<div class = "container">
			<span class = "d"> Phân khoa </span>
			<select name="khoa" id="khoa">
			<?php
				$arr = array("" => "", "MAT" => "Khoa học máy tính", "KDL" => "Khoa học vật liệu");
				foreach($arr as $key=>$value){ ?>
				<option value = "<?php echo $key; ?>" > <?php echo $value; ?> </option>
			<?php } ?>
			</select>
		</div>
		
		<div class = "container">
			<span class = "d"> Từ khoá </span>
			<input type="text" id="keytext" name="keytext">
		</div>
		<div class = "c">
			<button class = "button" name = "search">Tìm kiếm</button>
		</div>
	</form>
	<?php
	if (!isset($_POST["search"]) || (empty($_POST["khoa"]))&& empty($_POST["keytext"])){
		$tv="select id,name, faculty, address from student";
		$result=mysqli_query($connect, $tv);
		$tv_2 = "select count(id) as total from student";
		$result2=mysqli_query($connect, $tv_2);
		$data2=mysqli_fetch_assoc($result2);
	}
	else if (isset($_POST["search"])&& (!empty($_POST["khoa"]))&& (empty($_POST["keytext"]))){
		$tv="select id,name, faculty, address from student where faculty = '".$_POST["khoa"]."'";
		$result=mysqli_query($connect, $tv);
		$tv_2 = "select count(id) as total from student where faculty = '".$_POST["khoa"]."'";
		$result2=mysqli_query($connect, $tv_2);
		$data2=mysqli_fetch_assoc($result2);
	}
	else if (isset($_POST["search"])&& (empty($_POST["khoa"]))&& (!empty($_POST["keytext"]))){
		$tv="select id,name, faculty, address from student where name like '%".$_POST["keytext"]."%' or address like '%".$_POST["keytext"]."%'";
		$result=mysqli_query($connect, $tv);
		$tv_2 = "select count(id) as total from student where name like '%".$_POST["keytext"]."%' or address like '%".$_POST["keytext"]."%'";
		$result2=mysqli_query($connect, $tv_2);
		$data2=mysqli_fetch_assoc($result2);
	}
	else if (isset($_POST["search"])&& (!empty($_POST["khoa"]))&& (!empty($_POST["keytext"]))){
		$tv="select id,name, faculty, address from student where faculty = '%".$_POST["khoa"]."%' and name like '%".$_POST["keytext"]."%' or address like '%".$_POST["keytext"]."%'";
		$result=mysqli_query($connect, $tv);
		$tv_2 = "select count(id) as total from student where faculty = '%".$_POST["khoa"]."%' and name like '%".$_POST["keytext"]."%' or address like '%".$_POST["keytext"]."%'";
		$result2=mysqli_query($connect, $tv_2);
		$data2=mysqli_fetch_assoc($result2);
	}
	?>
	<p> Số sinh viên tìm thấy: <?php echo $data2['total']; ?> </p>
	<div class = "e">
		<button class = "button" onClick="document.location.href='signup.php'"> Thêm </button>
	</div>
	<table style="width:100%">
		<tr>
			<th>No</th>
			<th>Tên sinh viên</th>
			<th>Khoa</th>
			<th>Địa chỉ </th>
			<th>Action</th>
		</tr>
		<?php
		for($i = 1; $i <= $data2['total']; $i++){ 
			$arr = mysqli_fetch_array($result);?>
		<tr>
			<td width = "5%"><?php echo $arr['id']; ?></td>
			<td width = "20%"><?php echo $arr['name']; ?></td>
			<td width = "25%"><?php
				$a = array("" => "", "MAT" => "Khoa học máy tính", "KDL" => "Khoa học vật liệu");
				echo $a[$arr['faculty']]; ?></td>
			<td width = "30%"><?php echo $arr['address']; ?></td>
			<td width = "20%">
				<div class = "container">
					<button class = "button">Sửa</button>
					<button class = "button">Xoá</button>
				</div>
			</td>
		</tr>
		<?php } ?>
	</table>
</body>
</html>